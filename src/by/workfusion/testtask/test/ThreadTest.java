package by.workfusion.testtask.test;

import by.workfusion.testtask.service.SimpleThread;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Andrey on 07.07.2015
 */
public class ThreadTest {
    static {
        new DOMConfigurator().doConfigure("resources\\log4j.xml", LogManager.getLoggerRepository());
    }
    public static void main(String[] args) {
        SimpleThread simpleThread = new SimpleThread(0,1);
        simpleThread.run();
    }
}
