package by.workfusion.testtask.service;

import java.util.Random;

public class ThreadService {
    public static Random rnd = new Random();
    private int counter;
    private long max;
    private long min;
    private long sum;
    private long value;

    public ThreadService() {
        value = 0;
        max = value;
        min = value;
        sum = value;
        counter = 0;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public int getCounter() {
        return counter;
    }

    public long getSum() {
        return sum;
    }

    public long generate(){
        long tmp = rnd.nextLong();
        setValue(tmp);
        if ( tmp > getMax()){
            setMax(tmp);
        }
        if( tmp < getMin()){
            setMin(tmp);
        }
        sum += tmp;
        counter++;
        return value;
    }
}
