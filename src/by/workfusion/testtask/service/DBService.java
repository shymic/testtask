package by.workfusion.testtask.service;

import org.apache.log4j.Logger;

import java.sql.*;

/**
 * Created by Andrey on 07.07.2015
 */
public class DBService {
    private static final Logger LOG = Logger.getLogger(DBService.class);

    public static Connection connect() throws SQLException {
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        Connection cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/record",
                "root", "1111");
        return cn;
    }

    public static void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
           LOG.error(e);
        }
    }

}
