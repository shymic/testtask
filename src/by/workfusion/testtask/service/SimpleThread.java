package by.workfusion.testtask.service;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andrey on 07.07.2015
 */
public class SimpleThread implements Runnable{
    private static final Logger LOG = Logger.getLogger(SimpleThread.class);
    private static final String SQL_REQUEST = "INSERT INTO record VALUES (?,?,?,?,?)";
    private long value;
    private long timestamp;

    public SimpleThread() {
    }

    public SimpleThread(long value, long timestamp) {
        this.timestamp = timestamp;
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public void run() {
        ThreadService threadService = new ThreadService();
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            while (threadService.getCounter() < 10) {
                connection = DBService.connect();
                ps  = connection.prepareStatement(SQL_REQUEST);
                setValue(threadService.generate());
                ps.setInt(1, (int) getTimestamp() * threadService.getCounter());
                ps.setLong(2, getValue());
                ps.setLong(3, threadService.getMin());
                ps.setLong(4, threadService.getMax());
                ps.setDouble(5, (double) threadService.getSum() / threadService.getCounter());
                ps.executeUpdate();
                DBService.close(ps);
                LOG.info("added record");
                TimeUnit.SECONDS.sleep(getTimestamp());
            }
        } catch (InterruptedException e) {
            LOG.error(e);
        } catch (SQLException e) {
            LOG.error(e);
        }finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                LOG.error(e);
            }
        }

    }


}
